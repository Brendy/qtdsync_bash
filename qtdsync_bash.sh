#!/bin/bash
set -Euo pipefail

## (1) Argument logic - template from https://betterdev.blog/minimal-safe-bash-script-template/

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
}

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [OPTIONS]

Description:
This script is a wrapper for the command 'rsync --link-dest' to resemble the function of QtdSync by Thomas Döring.
It hardlinks existing copies of a previous backup in the new backup and copies only missing/changed files.
TARGET is the directory where datetime subdirectories are created with the content of SOURCE therein.

Options:
  -u, --source               [REQUIRED] Path of the source directory to backup from.
  -t, --target               [REQUIRED] Path of the target directory to backup to.
  -n, --dry_run              Only create a logfile of the rsync dry-run in TARGET.

EOF
  exit
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

parse_params() {
  # default values of variables set from params
  source=''
  target=''
  dry_run=''

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    -s | --source)
      source="$2"
      shift 2
      ;;
    -t | --target)
      target="$2"
      shift 2
      ;;
    -n | --dry-run)
      dry_run="--dry-run"
      shift 1
      ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
  done

  ## Debug vars
  #  source="/media/veracrypt2/Documents/Rezepte"
  #  target="/media/veracrypt3/Backup/2nd_HDD"
  #  target="/home/$USER/2nd_HDD"
  #  dry_run='--dry-run'

  ## check required params and arguments
  [[ -z "${source-}" ]] && die "Please provide a parameter for the argument --source. Try $(basename "${BASH_SOURCE[0]}") --help."
  [[ -z "${target-}" ]] && die "Please provide a parameter for the argument --target. Try $(basename "${BASH_SOURCE[0]}") --help."

  return 0
}

# Setup cleanup-on-exit function
trap cleanup SIGINT SIGTERM ERR EXIT

# Parse arguments
echo "For help, type $(basename "${BASH_SOURCE[0]}") --help"
echo ""
parse_params "$@"


## (2) Script logic

## datetime directory names
timestamp=$(date "+%Y.%m.%d_%T")
new_backup_datetime=${timestamp//":"/"."}
[[ -d "$target" ]] || mkdir -p "$target"
last_backup_datetime=$(find $target -mindepth 1 -maxdepth 1 | sort | tail -n1 | xargs -I {} basename {})

## derive subpaths
backup_dir=$(basename "$source")

## Do not hardlink on first run
if [ -z ${last_backup_datetime:+x} ]; then
  link_dest=""
else
  link_dest="--link-dest=../../${last_backup_datetime}/${backup_dir}"
fi

## Build rsync command
read -r -d '' cmd_array <<EOM
rsync
-rltv
$dry_run
--progress
--hard-links
--delete
--ignore-errors
--force
--log-file=${target}/${new_backup_datetime}/qtdsync_bash.log
--exclude=/cygdrive
$link_dest
${source}/
${target}/${new_backup_datetime}/${backup_dir}
EOM

## Print command
cmd_string=($(echo $cmd_array))
echo "Executing command:  ${cmd_string[@]}"
echo ""

## Run rsync
mkdir -p ${target}/${new_backup_datetime}/${backup_dir} && cd ${target}/${new_backup_datetime}/${backup_dir}
"${cmd_string[@]}"
echo "######################################## Executed Command" >>${target}/${new_backup_datetime}/qtdsync_bash.log
echo "${cmd_string[@]}" >>${target}/${new_backup_datetime}/qtdsync_bash.log
echo ""
echo "Please review the logfile: ${target}/${new_backup_datetime}/qtdsync_bash.log"

## Remove new directories if in dry run
if [ ! -z ${dry_run:+x} ]; then
  rm -Rf ${target}/${new_backup_datetime}/${backup_dir}
fi
