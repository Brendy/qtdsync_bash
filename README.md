# QtdSync_bash

Mimic QtdSync with a simple BASH script.

## Run

```shell
$ /bin/bash qtdsync_bash.sh --help
For help, type qtdsync_bash.sh --help

Usage: qtdsync_bash.sh [OPTIONS]

Description:
This script is a wrapper for the command 'rsync --link-dest' to resemble the function of QtdSync by Thomas Döring.
It hardlinks existing copies of a previous backup in the new backup and copies only missing/changed files.
TARGET is the directory where datetime subdirectories are created with the content of SOURCE therein.

Options:
-u, --source               [REQUIRED] Path of the source directory to backup from.
-t, --target               [REQUIRED] Path of the target directory to backup to.
-n, --dry_run              Only create a logfile of the rsync dry-run in TARGET.
```
